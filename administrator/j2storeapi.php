<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_J2storeapi
 * @author     sp selvakumar <sp.selvakumar2012@gmail.com>
 * @copyright  2019 sp selvakumar
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

use \Joomla\CMS\MVC\Controller\BaseController;
use \Joomla\CMS\Factory;
use \Joomla\CMS\Language\Text;

// Access check.
if (!Factory::getUser()->authorise('core.manage', 'com_j2storeapi'))
{
	throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('J2storeapi', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('J2storeapiHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'j2storeapi.php');

$controller = BaseController::getInstance('J2storeapi');
$controller->execute(Factory::getApplication()->input->get('task'));
$controller->redirect();
