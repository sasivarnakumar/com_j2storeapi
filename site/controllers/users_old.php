<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_J2storeapi
 * @author     sp selvakumar <sp.selvakumar2012@gmail.com>
 * @copyright  2019 sp selvakumar
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Users list controller class.
 *
 * @since  1.6
 */
class J2storeapiControllerUsers extends J2storeapiController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Users', $prefix = 'J2storeapiModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
