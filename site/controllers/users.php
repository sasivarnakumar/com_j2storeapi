<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_J2storeapi
 * @author     sp selvakumar <sp.selvakumar2012@gmail.com>
 * @copyright  2019 sp selvakumar
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Users list controller class.
 *
 * @since  1.6
 */
class J2storeapiControllerUsers extends J2storeapiController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *	@since	1.6
	 * 
	 */
	public function &getModel($name = 'Users', $prefix = 'J2storeapiModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
	/**	all users
	*Should need date from, date to, Limit
	*Example url: ?option=com_j2storeapi&view=users&task=users.getUsers&dateFrom=2000-01-01&dateTo=2019-06-30&limit=50
	*/
	public function getUsers()
	{	
		$config = \JComponentHelper::getParams('com_users');
		$jinput = JFactory::getApplication();
		$dateFrom=$jinput->input->get('dateFrom','2000-01-01');
		$dateTo=$jinput->input->get('dateTo','null');
		$limit=$jinput->input->get('limit','50');
		$conditions = array();
		if($dateTo == null){
			$dateTo= JFactory::getDate();
		}
		// Get a db connection.
		$db = JFactory::getDbo();
		// Create a new query object.
		$query = $db->getQuery(true);
		//Query generation
		$query->select($db->quoteName(array('id','name','username','email','registerDate','block')))
		->from($db->quoteName('#__users'))
		->where($db->quoteName('registerDate'). ' BETWEEN ' .  $db->quote($dateFrom). ' AND ' . $db->quote($dateTo))
		// ->where($db->quoteName('block'). ',' . '0' )
		->setLimit($limit);
		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$results = $db->loadObjectList();
		// return data
		return json_encode($results);
		
		
		
	}
	
// 
/**	Get user by id or user_name or user email
	*Should need user_id or user_name or email_id
	*Example url: ?option=com_j2storeapi&view=users&task=users.getUser&id=1
*/
	public function getUser()
	{	
		$config = \JComponentHelper::getParams('com_users');
		$jinput = JFactory::getApplication();
		$user_id=$jinput->input->get('id','');
		$user_name=$jinput->input->get('username','');
		$email=$jinput->input->get('email','');
		//Search column is id
		$column_name='id';
		$column_value=$user_id;
		// If id not passed in request
		if(empty($column_value)){
			// Search column is username
			$column_name='username';
			$column_value=$user_name;
			//If uname not passed
			if(empty($column_value))
			{
				//Search column is email
				$column_name='email';
				$column_value=$email;
			}
		}
		//Id not passed any arg
		if(empty($column_value)){
			print_r('No arguments passed');
			exit;
		}
		
		// Get a db connection.
		$db = JFactory::getDbo();
		// Create a new query object.
		$query = $db->getQuery(true);
		//Query generation
		$query->select($db->quoteName(array('id','name','username','email','registerDate','block')))
		->from($db->quoteName('#__users'))
		->where($db->quoteName($column_name). ' = ' . $db->quote($column_value));
		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$results = $db->loadObjectList();
		// echo '<pre>';
		// print_r($results);
		// exit;
		return json_encode($results);

		
	}

// 
/**	Delete user by id or user_name or user email
	*Should need user_id or user_name or email_id
	*Example url: ?option=com_j2storeapi&view=users&task=users.deleteUser&id=1
*/
	public function deleteUser()
	{	
		$config = \JComponentHelper::getParams('com_users');
		$jinput = JFactory::getApplication();
		$user_id=$jinput->input->get('id','');
		$user_name=$jinput->input->get('username','');
		$email=$jinput->input->get('email','');
		//Search column is id
		$column_name='id';
		$column_value=$user_id;
		// If id not passed in request
		if(empty($column_value)){
			// Search column is username
			$column_name='username';
			$column_value=$user_name;
			//If uname not passed
			if(empty($column_value))
			{
				//Search column is email
				$column_name='email';
				$column_value=$email;
			}
		}
		//Id not passed any arg
		if(empty($column_value)){
			print_r('No arguments passed');
			exit;
		}
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		// // delete all custom keys for user .
		$query->delete($db->quoteName('#__users'));
		$query->where($db->quoteName($column_name). ' = ' . $db->quote($column_value));
		$db->setQuery($query);
		$result = $db->execute();
		if(!$result){
			print_r('Record not found');
		exit;
		}
		//delete uer map
		// _user_usergroup_map
		$query->delete($db->quoteName('#__user_usergroup_map'));
		$query->where($db->quoteName('user_id'). ' = ' . $db->quote($user_id));
		$db->setQuery($query);
		$result = $db->execute();
		return json_encode($results);

		// print_r('Deleted');
		// exit;
	}
// 	users
// add new user
	public function addUser()
	{
		//loading instance		
		$instance = \JUser::getInstance();
		jimport('joomla.application.component.helper');
		$config = \JComponentHelper::getParams('com_users');
		$jinput = JFactory::getApplication();
		//User details
		$details=$jinput->input->post->getArray();
		$defaultUserGroup = $config->get('new_usertype', 2);
		if (isset($details['group_ids']))
		{
			$defaultUserGroup = array_pop($details['group_ids']);
		}
		//Password
		if (version_compare(JVERSION, '3.2.1', 'ge'))
		{
			$md5_pass = \JUserHelper::hashPassword($jinput->input->get('password',''));
		}
		else
		{
			$md5_pass = md5($details['password']);
		}
		//If user email exists
		$acl = \JFactory::getACL();
		$instance->set('id', 0);
		$instance->set('name', $details['name']);
		$instance->set('username', $details['email']);
		$instance->set('password', $md5_pass);
		$instance->set('email', $details['email']);
		  // Result should contain an email (check)
		$instance->set('usertype', 'deprecated');
		$instance->set('groups', array($defaultUserGroup));
		$activation_key = md5(rand(134000,999999));
		$instance->set('activation', $activation_key);
		$instance->set('block', 1);
		if (!$instance->save())
		{
			$error= \JError::raiseWarning('Registration fail', $instance->getError());
		}
		$useractivation = '1';
		// Send registration confirmation mail
		$this->_sendMail($instance, $details, $useractivation);
		// Send registration confirmation mail
		return json_encode($instance);
		// exit;
	}

	// 	User creation post curl
	public function createPostCall()
	{
	//extract data from the post
	//set POST variables
	$url = 'http://localhost/my_joom/?option=com_j2storeapi&view=users&task=users.addUser';
	$fields = array(
		'name' => urlencode('spselva'),
		'email' => urlencode('selvakumar2012@gmail.com'),
		'username' => urlencode('selvakumar2012@gmail.com'),
		'password' => urlencode('monkatwork'),
		// 'id' => urlencode('601')
	);
	//url-ify the data for the POST
	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string, '&');
	//open connection
	$ch = curl_init();
	//set the url, number of POST vars, POST data
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//execute post
	$result = curl_exec($ch);
	print_r($result);
	exit;
	//close connection
	curl_close($ch);
	}
	// 	users
	// update new user
	public function updateUser()
	{
		//loading instance		
		$instance = \JUser::getInstance();
		jimport('joomla.application.component.helper');
		$config = \JComponentHelper::getParams('com_users');
		$jinput = JFactory::getApplication();
		$details=$jinput->input->post->getArray();
		$user=\JFactory::getUser($details['id']);
		if($user->id == 0){
			print_r('Passed user id not valid');
		}

		//User details
		$defaultUserGroup = $config->get('new_usertype', 2);
		if (isset($details['group_ids']))
		{
			$defaultUserGroup = array_pop($details['group_ids']);
		}
		//Password
		if (version_compare(JVERSION, '3.2.1', 'ge'))
		{
			$md5_pass = \JUserHelper::hashPassword($jinput->input->get('password',''));
		}
		else
		{
			$md5_pass = md5($details['password']);
		}
		//If user email exists
		$acl = \JFactory::getACL();
		
		$instance->set('username', $user->username);

		if(!$details['id']){
			print_r('Error User id not found');
			// exit;
		}
		$instance->set('id', $details['id']);
		//isset name
		if(isset($details['name'])){
		$instance->set('name', $details['name']);
		}
		//isset password
		if(isset($details['password'])){
		$instance->set('password', $md5_pass);
		}
		//isset email
		if(isset($details['password'])){
			$instance->set('email', $details['email']);
		}
		if(isset($details['group_ids'])){
			$instance->set('groups', array($defaultUserGroup));
		}
		if (!$instance->save())
		{
			$error= \JError::raiseWarning('Registration fail', $instance->getError());
		}
		return json_encode($instance);
	}
	/**
	 * Returns yes/no
	 *
	 * @param object
	 * @param mixed Boolean
	 *
	 * @return array
	 */
	function _sendMail(&$user, $details, $useractivation)
	{
		$mainframe = \JFactory::getApplication();
		$config    = \JFactory::getConfig();

		$name = $user->get('name');
		if (empty($name))
		{
			$name = $user->get('email');
		}
		$email      = $user->get('email');
		$username   = $user->get('username');
		$activation = $user->get('activation');
		$password   = $details['password2']; // using the original generated pword for the email

		$usersConfig = \JComponentHelper::getParams('com_users');
		// $useractivation = $usersConfig->get( 'useractivation' );
		$sitename = $config->get('sitename');
		$mailfrom = $config->get('mailfrom');
		$fromname = $config->get('fromname');
		$siteURL  = \JURI::base();

		$subject = \JText::sprintf('COM_SWIMFEE_ACCOUNT_DETAILS', $name, $sitename);
		$subject = html_entity_decode($subject, ENT_QUOTES);

		$send_password = $usersConfig->get('sendpassword', 0);
		if ($useractivation == 1)
		{
			$message = \JText::sprintf('COM_SWIMFEE_SEND_MSG_ACTIVATE', $name, $sitename, $siteURL . "index.php?option=com_users&task=registration.activate&token=" . $activation, $siteURL, $email, $password);
		}
		else
		{
			if ($send_password)
			{
				$message = \JText::sprintf('COM_SWIMFEE_SEND_MSG', $name, $sitename, $siteURL, $email, $password);
			}
			else
			{
				$message = \JText::sprintf('COM_SWIMFEE_SEND_MSG_NOPW', $name, $sitename, $siteURL, $email);
			}
		}

		$message = html_entity_decode($message, ENT_QUOTES);

		$success = $this->_doMail($mailfrom, $fromname, $email, $subject, $message);

		return $success;
	}
	/**
	 *
	 * @return unknown_type
	 */
	function _doMail($from, $fromname, $recipient, $subject, $body, $actions = null, $mode = null, $cc = null, $bcc = null, $attachment = null, $replyto = null, $replytoname = null)
	{
		$success = false;

		$message = \JFactory::getMailer();
		$message->addRecipient($recipient);
		$message->setSubject($subject);
		$message->setBody($body);
		$sender = array($from, $fromname);
		$message->setSender($sender);
		try
		{
			$sent = $message->send();
		}
		catch (Exception $e)
		{
			//do nothing. Joomla has botched up the entire mail system from 3.5.1
		}

		if ($sent == '1')
		{
			$success = true;
		}

		return $success;
	}


}



