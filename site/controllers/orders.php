<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_J2storeapi
 * @author     sp selvakumar <sp.selvakumar2012@gmail.com>
 * @copyright  2019 sp selvakumar
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Users list controller class.
 *
 * @since  1.6
 */
class J2storeapiControllerOrders extends J2storeapiController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Orders', $prefix = 'J2storeapiModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
	/**	Fetching all orders lists
	*Should need date from, date to, Limit
	*Example url: ?option=com_j2storeapi&view=users&task=orders.getOrders&dateFrom=2000-01-01&dateTo=2019-06-30&limit=2
	*/
	public function getOrders()
	{		
		$config = \JComponentHelper::getParams('com_users');
		$jinput = JFactory::getApplication();
		//collect information from get 
		$dateFrom=$jinput->input->get('dateFrom','2000-01-01');
		$dateTo=$jinput->input->get('dateTo','null');
		$limit=$jinput->input->get('limit','50');
		if($dateTo == null){
			$dateTo= JFactory::getDate();
		}
		//Create DB connection
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		//Create query joining the all related tables 
		$query = "SELECT oi.*,jp.*,c.title,o.*,os.*,oif.* FROM #__j2store_orderitems as oi				
				LEFT JOIN #__j2store_products as jp ON oi.product_id = jp.j2store_product_id
				LEFT JOIN #__content as c ON c.id = jp.product_source_id
				LEFT JOIN #__j2store_orders as o ON o.order_id = oi.order_id
				LEFT JOIN #__j2store_orderinfos as oif ON oif.order_id = oi.order_id
				LEFT JOIN #__j2store_orderstatuses as os ON os.j2store_orderstatus_id = o.order_state_id 
				where  o.created_on BETWEEN '" .$dateFrom . "' AND '" .$dateTo. "'
				limit ". $limit." ";
		//execute query			
		$db->setQuery($query);
		$orders= $db->loadObjectList();	
		// echo '<pre>';
		// print_r($orders);
		// exit;
		if(empty($orders)){
			$order='No record found';
		}	
		
		return json_encode($orders);
 	}
	
	/**	Get Order by order_id 
		*Should need order_id 
		*Example url: ?option=com_j2storeapi&view=users&task=orders.getOrder&order_id=1
	*/
	public function getOrder()
	{	
	
		$config = \JComponentHelper::getParams('com_users');
		$jinput = JFactory::getApplication();
		$order_id=$jinput->input->get('order_id','');
		//Create DB connection
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		//Create query joining the all related tables 
		$query = "SELECT oi.*,jp.*,c.title,o.*,os.*,oif.* FROM #__j2store_orderitems as oi				
				LEFT JOIN #__j2store_products as jp ON oi.product_id = jp.j2store_product_id
				LEFT JOIN #__content as c ON c.id = jp.product_source_id
				LEFT JOIN #__j2store_orders as o ON o.order_id = oi.order_id
				LEFT JOIN #__j2store_orderinfos as oif ON oif.order_id = oi.order_id
				LEFT JOIN #__j2store_orderstatuses as os ON os.j2store_orderstatus_id = o.order_state_id 
				where oi.order_id=".$order_id ;
		//execute query			
		$db->setQuery($query);
		$order= $db->loadObjectList();	
		// 	echo '<pre>';
		// print_r($order);
		// exit;
		if(empty($order)){
			$order='No record found';
		}		
		return json_encode($order);		
	}
}



